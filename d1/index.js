
const express = require('express');
const app = express();
const port = 3001;
//Mongoose is a package that allows creation of Schemas to model our data structures.
//Also has access to a number of  methods for manipulation of our database.

const mongoose = require("mongoose")
//MongoDB Atlas Connection
//when we want to use MongoDB/robo3t
//mongoose.connect("mongodb://localhost:27017/databaseName")
mongoose.connect("mongodb+srv://jdmendame:jestandalemendame@jdmendame.7de3z.mongodb.net/batch164_to-do?retryWrites=true&w=majority",
      {

           useNewUrlParser: true,
           useUnifiedTopology: true
      }
)


let db = mongoose.connection

//connection error message
db.on("error", console.error.bind(console, "connection error"))
//connection is successful message
db.once("open",()=> console.log("We're connected to the cloud database."))


app.use(express.json());
app.use(express.urlencoded({ extended:true }));
app.listen(port, ()=> console.log(`Server running at port:${port}`));


//Mongoose Schemas
//Schemas determines the structure of the documents to be written in the database.
//act as a blueprint to our data
//Use Schema() constructor of the Mongoose module to create a new Schema object.


const taskSchema = new mongoose.Schema({

	name: String,
	status: {
		type: String,
		//Default values are the predefined valoues for a field if we don't put any value
        default: "pending"	
  
	}
})

const Task = mongoose.model("Task", taskSchema)

//Routes/endpoints

//Creating a new task

//Business Logic

/*

1. Add a functionality to check if there are duplicate tasks

     -If the task already exists in the database, we return error.
     -If the task doesn't exist in the database, we add it in the database.
           1. The task data will be coming from the request's body.
           2. Create a new Task object.
           3. Save the new objet to our database.
*/

app.post("/tasks", (req,res) =>   {
     Task.findOne(
    {name: req.body.name}, (err, result) => {
    //If a document was found and the document's name matches the information sent via the client, it will return an error.
    
    if(result != null && result.name == req.body.name){
    	return res.send("Duplicate task found")
    } else{
           //if no document was found
           //create a new task and save it to the database
           let newTask = new Task({
           	name: req.body.name
           });

            newTask.save((saveErr, savedTask) => {
                    //if there are errors in saving
                    if(saveErr){
                         return console.error(saveErr)
                    } else {
                         return res.status(201).send("New task created")
                    }
               })


          }
     })
})

//Get all tasks
//Business Logic

/*

1. Find/retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client and return and array of documents.

*/

app.get("/tasks", (req,res) =>{

Task.find({}, (err,result) => {

     if(err) {
          return console.log(err);
     } else {
          return res.status(200).json({
             dataFromMDB: result  
          })
     }

})

})