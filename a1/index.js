
const express = require('express');
const app = express();
const port = 3005;
const mongoose = require("mongoose")
mongoose.connect("mongodb+srv://jdmendame:jestandalemendame@jdmendame.7de3z.mongodb.net/batch164_to-do?retryWrites=true&w=majority",
      {

           useNewUrlParser: true,
           useUnifiedTopology: true
      }
)


let db = mongoose.connection

db.on("error", console.error.bind(console, "connection error"))
db.once("open",()=> console.log("We're connected to the cloud database."))


app.use(express.json());
app.use(express.urlencoded({ extended:true }));
app.listen(port, ()=> console.log(`Server running at port:${port}`));


//1. Create a User schema.

const userSchema = new mongoose.Schema({

	name: String,
	status: {
		type: String,
        default: "pending"},

    email: String,
    status: {
		type: String,
        default: "pending"}


})

//2. Create a User model.

const User = mongoose.model("User", userSchema)



//3. Create a POST route that will access the "/signup" route that will create a user.
//4. Process a POST request at the "/signup" route using postman to register a user.

app.post("/signup", (req,res) =>   {
     User.find(
    {name: req.body.name , email:req.body.email}, (err, result) => {
    
    if(result != null && result.name == req.body.name && result.email == req.body.email ){
    	return res.send("Duplicate user found")
    } else{
           
           let newUser = new User({
           	name: req.body.name,
           	email: req.body.email
           });

            newUser.save((saveErr, savedUser) => {
                    
                    if(saveErr){
                         return console.error(saveErr)
                    } else {
                         return res.status(201).send("User registered")
                    }
               })


          }
     })
})

//5. Create a GET route that will return all users.
//6. Process a GET request at the "/users" route using postman.

app.get("/user", (req,res) =>{

User.find({}, (err,result) => {

     if(err) {
          return console.log(err);
     } else {
          return res.status(200).json({
             dataFromMDB: result  
          })
     }

})

})





/*
1. Add a functionality to check if there are duplicate tasks

     -If the task already exists in the database, we return error.
     -If the task doesn't exist in the database, we add it in the database.
           1. The task data will be coming from the request's body.
           2. Create a new Task object.
           3. Save the new objet to our database.
*/




/*

Activity:




7. Create a git repository named S30.
8. Initialize a local git repository, add the remote link and push to git with the commit message of s30 Activity.
9. Add the link in Boodle named Express js Data Persistence via Mongoose ODM.


Business Logic in Creating a user "/signup"
1. Add a functionality to check if there are duplicates tasks
        - if the user already exists, return error Or "Already registered"
        - if the user does not exist, we add it on our database
                1. The user data will be coming from the req.body
                2. Create a new User object with a "username" and "password" fields/properties
                3. Then save, and add an error handling

 */